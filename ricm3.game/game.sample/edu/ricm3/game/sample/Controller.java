/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.Button;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedList;
import java.util.Random;

import edu.ricm3.game.GameController;

/**
 * This class is to illustrate the most simple game controller. It does not
 * much, but it shows how to obtain the key strokes, mouse buttons, and mouse
 * moves.
 * 
 * With ' ', you see what you should never do, SLEEP. With '+' and '-', you can
 * add or remove some simulated overheads.
 * 
 * @author Pr. Olivier Gruber
 */

public class Controller extends GameController implements ActionListener {

  Model m_model;
  View m_view;
 // Button m_cowboysOn;
  Button m_explosionsOn;
//  Button m_strobesOn;
  Button m_plus, m_minus;
  Music m_player;
  Music m_effect;
  Random rand = new Random();
  //int codeKey;

  public Controller(Model model, View view) {
    m_model = model;
    m_view = view; 
  }

  /**
   * Simulation step. Warning: the model has already executed its step.
   * 
   * @param now
   *          is the current time in milliseconds.
   */
  @Override
  public void step(long now) {
    m_model.step(now);
    m_view.step(now);
  }

  @Override
  public void keyTyped(KeyEvent e) {
//    if (Options.ECHO_KEYBOARD)
//      System.out.println("KeyTyped: " + e);
   /* if (e.getKeyChar() == ' ') {
      try {
        /*
         * NEVER, EVER, DO THIS!
         * NEVER LOOP FOR LONG, NEVER BLOCK, OR NEVER SLEEP,
         * YOU WILL BLOCK EVERYTHING.
         */
    /*    System.err.println("You should not have done that!");
        System.out.println("ZZzzz....");
        Thread.sleep(3000);
        System.out.println("Hey! I am back");
      } catch (InterruptedException ex) {
      }
    } else if (e.getKeyChar() == '+') {
      Overhead h = m_model.getOverhead();
      h.inc();
    } else if (e.getKeyChar() == '-') {
      Overhead h = m_model.getOverhead();
      h.dec();
    }
    */
  }

  @Override
  public void keyPressed(KeyEvent e) {
	if (m_model.m_cowboys.auSol() || m_model.codeKey == 0x25 || m_model.codeKey == 0x27) {
		//m_model.oldCodeKey = m_model.codeKey;
		m_model.codeKey = e.getKeyCode();
	}
	
	if (e.getKeyCode() == 0x26) {
		//m_effect.start();
	}
	
    if (Options.ECHO_KEYBOARD)
      System.out.println("KeyPressed: " + e.getKeyChar() + " code=" + e.getKeyCode());
  }

  @Override
  public void keyReleased(KeyEvent e) {
    if (Options.ECHO_KEYBOARD)
      System.out.println("KeyReleased: " + e.getKeyChar() + " code=" + e.getKeyCode());
  }

  @Override
  public void mouseClicked(MouseEvent e) {
	  
	if (m_model.gameMode == 0) {
		m_model.gameMode = 1;
		m_player.stop();
		File file = new File("game.sample/sprites/ost2.wav");
		try {
			m_player = new Music(file);
			m_player.start();
		} catch (Exception ex) {
		}
		m_player.start();
	}
	
	if (m_model.gameMode == 2) {
		
		m_model.m_ghosts =  new LinkedList<Ghost>();
	    for (int i = 0; i < Options.NGHOSTS; i++)
	        m_model.m_ghosts.add(new Ghost(m_model, i, m_model.m_ghostSprite, 4, 4, rand.nextInt(700), rand.nextInt(200), 2F));
	    
	    m_model.m_cowboys.m_x = 0;
	    m_model.m_cowboys.m_y = 300;
	    m_model.m_cowboys.onPlat = false;
	    m_model.m_cowboys.floor = m_model.m_floor - (m_model.m_cowboys.m_h*(int)m_model.m_cowboys.m_scale) - 5;
	    m_model.m_cowboys.NBHEARTS = Options.NHEARTS;
		m_model.gameMode = 1;
		
		m_player.stop();
		File file = new File("game.sample/sprites/ost2.wav");
		try {
			m_player = new Music(file);
			m_player.start();
		} catch (Exception ex) {
		}
		m_player.start();
	}
	
	
    if (Options.ECHO_MOUSE)
      System.out.println("MouseClicked: (" + e.getX() + "," + e.getY() + ") button=" + e.getButton());
  }

  @Override
  public void mousePressed(MouseEvent e) {
    if (Options.ECHO_MOUSE)
      System.out.println("MousePressed: (" + e.getX() + "," + e.getY() + ") button=" + e.getButton());
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    if (Options.ECHO_MOUSE)
      System.out.println("MouseReleased: (" + e.getX() + "," + e.getY() + ") button=" + e.getButton());
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    if (Options.ECHO_MOUSE_MOTION)
      System.out.println("MouseEntered: (" + e.getX() + "," + e.getY());
  }

  @Override
  public void mouseExited(MouseEvent e) {
    if (Options.ECHO_MOUSE_MOTION)
      System.out.println("MouseExited: (" + e.getX() + "," + e.getY());
  }

  @Override
  public void mouseDragged(MouseEvent e) {
    if (Options.ECHO_MOUSE_MOTION)
      System.out.println("MouseDragged: (" + e.getX() + "," + e.getY());
  }

  @Override
  public void mouseMoved(MouseEvent e) {
    if (Options.ECHO_MOUSE_MOTION) {}
      //System.out.println("MouseMoved: (" + e.getX() + "," + e.getY());
  }

  public void notifyVisible() {
	  System.out.println("cc");
    Container cont = new Container();
    //cont.setLayout(new FlowLayout());
/* 
    m_explosionsOn = new Button("X");
    m_explosionsOn.addActionListener(this);
    cont.add(m_explosionsOn);
*/
    File file;
    file = new File("game.sample/sprites/ost1.wav");
    try {
      m_player = new Music(file);
      m_player.start();
    } catch (Exception ex) {
    }
    
    file = new File("game.sample/sprites/jump.wav");
    try {
      m_effect = new Music(file);
      m_effect.setLevel((float)0.1);
    } catch (Exception ex) {
    }
    
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    Object s = e.getSource();
    /*if (s == m_strobesOn)
      Options.STROBBING_SQUARES = !Options.STROBBING_SQUARES;
    /*else if (s == m_cowboysOn)
      Options.SHOW_COWBOYS = !Options.SHOW_COWBOYS;*/
    if (s == m_explosionsOn)
      Options.EXPLODE_COWBOYS = !Options.EXPLODE_COWBOYS;
    else if (s == m_plus && Options.SHOW_NCOWBOYS < Options.MAX_NCOWBOYS)
      Options.SHOW_NCOWBOYS++;
    else if (s == m_minus && Options.SHOW_NCOWBOYS > 0)
      Options.SHOW_NCOWBOYS--;
  }

}
