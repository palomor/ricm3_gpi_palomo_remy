package edu.ricm3.game.sample;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Ghost {

	//ATTRIBUTS
	BufferedImage m_sprite;
	int m_w, m_h;
	int m_x, m_y;
	int m_nrows, m_ncols;
	int m_step;
	int m_nsteps;
	int m_idx;
	float m_scale;
	long m_lastMove, m_lastReverse;
	int moovStart;
	int nextMoov;
    int NBSTEPS = 150;
	boolean firstMoov = true;
	
	//boolean m_canExplode;
	//boolean m_explode;
	BufferedImage[] m_sprites;
	//Explosion m_explosion;
	Model m_model;
	Cowboy target;
	int widthWindow;
	int heightWindow;
	
	// CONSTRUCTEUR
	Ghost(Model model, int no, BufferedImage sprite, int rows, int columns, int x, int y, float scale) {
	    m_model = model;
	    target = model.m_cowboys;
	    m_sprite = sprite;
	    m_ncols = columns;
	    m_nrows = rows;
	    m_x = x;
	    m_y = y;
	    m_scale= scale;
	    splitSprite();
    }
	
	// MÉTHODES
	
	void splitSprite() {
		int width = m_sprite.getWidth(null);
		int height = m_sprite.getHeight(null);
		m_sprites = new BufferedImage[m_nrows * m_ncols];
		m_w = width / m_ncols;
		m_h = height / m_nrows;
		m_step = 8;
		for (int i = 0; i < m_nrows; i++) {
			for (int j = 0; j < m_ncols; j++) {
				int x = j * m_w;
				int y = i * m_h;
				m_sprites[(i * m_ncols) + j] = m_sprite.getSubimage(x, y, m_w, m_h);
			}
		}
	 }
	
	void step(long now) {
		Random rand = new Random();
		moovStart = rand.nextInt(8);
		long elapsed = now - m_lastMove;
		
	    if (elapsed > 50L) {
	      m_lastMove = now;
	      
	      if (firstMoov) {
	    	  nextMoov = moovStart;
	    	  firstMoov = false;
	      }
	      else {
	    	  if (NBSTEPS > 0 && m_x >= 0 && m_x <= widthWindow-(m_w*m_scale) && (m_y >= 0 && m_y <= heightWindow-(m_h*m_scale))) {
	    		  switch (nextMoov) {
			      	case 0: 
				        m_x += 1;
				        m_idx = 8;
				        NBSTEPS--;
				    break;
				    
			      	case 1: 
				        m_x -= 1;
				        m_idx = 4;
				        NBSTEPS--;
				    break;
				    
			      	case 2: 
				        m_y -= 1;
				        m_idx = 0;
				        NBSTEPS--;
				    break;
				    
			      	case 3: 
				        m_y += 1;
				        m_idx = 0;
				        NBSTEPS--;
				    break;
				    
			      	case 4: 
				        m_x += 1;
				        m_y -= 1;
				        m_idx = 8;
				        NBSTEPS--;
				    break;
				    
			      	case 5: 
				        m_x -= 1;
				        m_y += 1;
				        m_idx = 4;
				        NBSTEPS--;
				    break;
				    
			      	case 6: 
				        m_x += 1;
				        m_y += 1;
				        m_idx = 8;
				        NBSTEPS--;
				    break;
				    
			      	case 7: 
				        m_x -= 1;
				        m_y -= 1;
				        m_idx = 4;
				        NBSTEPS--;
				    break;	
		      }
	    		  // TEST SI L'ON A TOUCHÉ LE COWBOY
					if ((m_x + m_w * (int) m_scale - 20 >= target.m_x + 20) && (m_y + m_h * (int) m_scale >= target.m_y)
							&& (m_x + m_w * (int) m_scale - 20 <= target.m_x + target.m_w * (int) target.m_scale)
							&& (m_y + m_h * (int) m_scale <= target.m_y + target.m_h * (int) target.m_scale)) {
						target.NBHEARTS--;
					}
					
					if (target.NBHEARTS == 0) {
						m_model.gameMode = 2;
					}
	    	 } else {
	    		 NBSTEPS = 150;
	    		 
	    		 if (m_x < 0 || m_y < 0) {
	    			 
	    			 if (m_x < 0) {
	    				m_x += 2; 
	    			 }
	    	    	 if (m_y < 0) {
	    	    		 m_y += 2;
	    	    	 }
	    	    	 
	    	     } else if (m_x > widthWindow-(m_w*m_scale) || m_y > heightWindow-(m_h*m_scale)) {
	    	    	 
	    	    	 if (m_x > widthWindow-(m_w*m_scale)) {
	    	    		 m_x -= 2;
	    	    	 }
	    	    	 if (m_y > heightWindow-(m_h*m_scale)) {
	    	    		 m_y -= 2;
	    	    	 } 
	    	     }
	    		 
	    		 firstMoov = true; 
	    	 }
	      }
	    }
	}
	
	
	
	void paint(Graphics g) {
		Image img = m_sprites[m_idx];
		int w = (int) (m_scale * m_w);
		int h = (int) (m_scale * m_h);
		g.drawImage(img, m_x, m_y, w, h, null);
		//System.out.println("Position : " + m_x + " " + m_y);
   }
}
