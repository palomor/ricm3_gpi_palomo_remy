/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.imageio.ImageIO;

import edu.ricm3.game.GameModel;

public class Model extends GameModel {
  LinkedList<Ghost> m_ghosts;
  LinkedList<Platform> m_platforms;
  LinkedList<Bullet> m_bullets;
  
  BufferedImage m_cowboySprite;
  BufferedImage m_explosionSprite;
  BufferedImage m_ghostSprite;
  BufferedImage m_bg1;
  BufferedImage m_bg2;
  BufferedImage m_sol;
  BufferedImage m_alphabet;
  BufferedImage m_alphabetBLUE;
  BufferedImage m_heart;
  BufferedImage m_bullet;
  
  Cowboy m_cowboys;
  Random rand = new Random();
  Overhead m_overhead = new Overhead();
  
  int codeKey;
  int oldCodeKey;
  int gameMode; // gameMode = 0 --> Menu Principal | gameMode = 1 --> Partie en cours | gameMode = 2 --> GAME OVER
  
  int m_floor = 580;
  int nsauts;

  
  public Model() {
    loadSprites();
    
    m_cowboys = new Cowboy(this, 0, m_cowboySprite, 4, 6, 0, 300, 2.5F);
    
    m_platforms = new LinkedList<Platform>();
    m_bullets = new LinkedList<Bullet>();
    
    m_ghosts = new LinkedList<Ghost>();
    for (int i = 0; i < Options.NGHOSTS; i++)
      m_ghosts.add(new Ghost(this, i, m_ghostSprite, 4, 4, rand.nextInt(700), rand.nextInt(200), 2F));
    
    nsauts = 0;
    gameMode = 0;
  }
  
  @Override
  public void shutdown() {
    
  }

  public Overhead getOverhead() {
    return m_overhead;
  }

  public Cowboy cowboys() {
    return m_cowboys;
  }
  
  public Iterator<Ghost> ghosts() {
	    return m_ghosts.iterator();
  }
  
  public Iterator<Platform> platforms() {
	    return m_platforms.iterator();
 }
  public Iterator<Bullet> bullets() {
	    return m_bullets.iterator();
 }
  
  /**
   * Simulation step.
   * 
   * @param now
   *          is the current time in milliseconds.
   */
  @Override
  public void step(long now) {
    
    m_overhead.overhead();
      
   // System.out.println("codeKey = " + codeKey);
   // System.out.println("oldCodeKey = " + oldCodeKey);
	
	switch (codeKey) {
	
	case 0x25:
		m_cowboys.goLeft(now);
		oldCodeKey = codeKey;
		codeKey = 0;
		break;
		
	case 0x27:
		m_cowboys.goRight(now);
		oldCodeKey = codeKey;
		codeKey = 0;
		break;
		
	case 0x26:
		if (m_cowboys.auSol()) {
				codeKey = 1000;
		}
		break;
		
	case 0x20:
		m_cowboys.shoot(now);
		codeKey = 0;
		break;
		
	case 1000:
		if (nsauts < 30) {
			m_cowboys.goJump(now, nsauts);
			nsauts++;
		} else {
			nsauts = 0;
			codeKey = 0;
		}
		break;
				
	default:
		m_cowboys.step(now);
		//oldCodeKey = 0;
	}
    
    Iterator<Ghost> iterG = m_ghosts.iterator();
    while (iterG.hasNext()) {
      Ghost g = iterG.next();
      g.step(now);
    }
    
    Iterator<Bullet> iterB = m_bullets.iterator();
    while (iterB.hasNext()) {
      Bullet b = iterB.next();
      b.step(now);
    }
  }
  

  
  private void loadSprites() {
    /*
     * Cowboy with rifle, western style; png; 48x48 px sprite size
     * Krasi Wasilev ( http://freegameassets.blogspot.com)
     */
    File imageFile = new File("game.sample/sprites/winchester.png");
    try {
      m_cowboySprite = ImageIO.read(imageFile);
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(-1);
    }
    
    /* Ghosts sprite
    * 
    */
   imageFile = new File("game.sample/sprites/ghost.png");
   try {
     m_ghostSprite = ImageIO.read(imageFile);
   } catch (IOException ex) {
     ex.printStackTrace();
     System.exit(-1);
   }
    
    
    /*
     * Long explosion set; png file; 64x64 px sprite size
     * Krasi Wasilev ( http://freegameassets.blogspot.com)
     */
    imageFile = new File("game.sample/sprites/explosion01_set_64.png");
    try {
      m_explosionSprite = ImageIO.read(imageFile);
    } catch (IOException ex) {
      ex.printStackTrace();
      System.exit(-1);
    }
    
    imageFile = new File("game.sample/sprites/ciel&lune.png");
	try {
		m_bg1 = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}

	imageFile = new File("game.sample/sprites/mountains.png");
	try {
		m_bg2 = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}

	imageFile = new File("game.sample/sprites/floor.png");
	try {
		m_sol = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}
	
	imageFile = new File("game.sample/sprites/Font Pixels.png");
	try {
		m_alphabet = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}

	imageFile = new File("game.sample/sprites/Font Pixels BLUE.png");
	try {
		m_alphabetBLUE = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}
	
	imageFile = new File("game.sample/sprites/heart.png");
	try {
		m_heart = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}
	
	imageFile = new File("game.sample/sprites/bullet.png");
	try {
		m_bullet = ImageIO.read(imageFile);
	} catch (IOException ex) {
		ex.printStackTrace();
		System.exit(-1);
	}
  }

}
