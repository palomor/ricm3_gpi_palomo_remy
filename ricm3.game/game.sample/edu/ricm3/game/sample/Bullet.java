package edu.ricm3.game.sample;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

public class Bullet {
	private static final long serialVersionUID = 1L;
	Color color = Color.DARK_GRAY;
	int b_x, b_y;
	int b_w, b_h;
	int codeKey;
	Image img;
	long m_lastMove;
	int widthWindow;
	Model m_model;
	
	public Bullet(Model m, int x, int y, int w, int h, int c) {
		this.m_model = m;
		this.b_x = x;
		this.b_y = y;
		this.b_w = w;
		this.b_h = h;
		this.codeKey = c;
	}
	
	protected void _paint(Graphics g) {
		g.drawImage(img, b_x, b_y, b_w, b_h, null);
	}
	
	void step(long now) {
		long elapsed = now - m_lastMove;
		
	    if (elapsed > 50L) {
	    	m_lastMove = now;
	    	if (codeKey == 25) {
	    		b_x -= 10;
	    		if (b_x < 0) {
	    			m_model.m_bullets.remove();
	    		}
	    	} else {
	    		b_x += 10;
	    		if (b_x > widthWindow) {
	    			m_model.m_bullets.remove();
	    		}
	    	}
	    }

	}
}
