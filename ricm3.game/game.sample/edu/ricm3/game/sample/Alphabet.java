package edu.ricm3.game.sample;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;


public class Alphabet {
	//ATTRIBUTS
	BufferedImage m_sprite;
	int m_w, m_h;
	int m_x, m_y;
	int m_nrows, m_ncols;
	int m_idx;
	float m_scale;
	
	BufferedImage[] m_sprites;
	Model m_model;
	int widthWindow;
	int heightWindow;
	
	// CONSTRUCTEUR
	Alphabet(Model model, int no, BufferedImage sprite, int rows, int columns, int x, int y, float scale) {
	    m_model = model;
	    m_sprite = sprite;
	    m_ncols = columns;
	    m_nrows = rows;
	    m_x = x;
	    m_y = y;
	    m_scale= scale;
	    m_idx = no;
	    splitSprite();
    }
	
	
	// MÉTHODES
	void splitSprite() {
		int width = m_sprite.getWidth(null);
		int height = m_sprite.getHeight(null);
		m_sprites = new BufferedImage[m_nrows * m_ncols];
		m_w = width / m_ncols;
		m_h = height / m_nrows;
		
		for (int i = 0; i < m_nrows; i++) {
			for (int j = 0; j < m_ncols; j++) {
				int x = j * m_w;
				int y = i * m_h;
				m_sprites[(i * m_ncols) + j] = m_sprite.getSubimage(x, y, m_w, m_h);
			}
		}
	}
	
	void paint(Graphics g) {
		Image img = m_sprites[m_idx];
		int w = (int) (m_scale * m_w);
		int h = (int) (m_scale * m_h);
		g.drawImage(img, m_x, m_y, w, h, null);
		//System.out.println("Position : " + m_x + " " + m_y);
   }
}
