package edu.ricm3.game.sample;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;


import edu.ricm3.game.GameView;

public class Platform extends GameView {
	private static final long serialVersionUID = 1L;
	Color color = Color.DARK_GRAY;
	int p_x, p_y;
	int p_w, p_h;
	Image img;
	
	
	public Platform(int x, int y, int w, int h) {
		this.p_x = x;
		this.p_y = y;
		this.p_w = w;
		this.p_h = h;
	}
	
	@Override
	protected void _paint(Graphics g) {
		g.drawImage(img, p_x, p_y, p_w, p_h, null);
	}
}
