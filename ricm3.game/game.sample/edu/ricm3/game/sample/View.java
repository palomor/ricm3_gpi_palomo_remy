/*
 * Educational software for a basic game development
 * Copyright (C) 2018  Pr. Olivier Gruber
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.ricm3.game.sample;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;

import edu.ricm3.game.GameView;

public class View extends GameView {

  private static final long serialVersionUID = 1L;

  Color m_background = Color.white;
  long m_last;
  int m_npaints;
  int m_fps;
  Model m_model;
  int last_y;
  boolean startGame = true;
  int GHOST[] = new int[]{6, 7, 14, 18, 19};
  int HUNTER[] = new int[]{7, 20, 13, 19, 4, 17};
  int CLICK[] = new int[]{2, 11, 8, 2, 10};
  int ANYWHERE[] = new int[]{0, 13, 24, 22, 7, 4, 17, 4};
  int GAME[] = new int[]{6, 0, 12, 4};
  int OVER[] = new int[]{14, 21, 4, 17};
  int TIME = 0;
  // Controller m_ctr;
  
  public View(Model m) {
    m_model = m;
    // m_ctr = c;
  }
  
  public void step(long now) {

  }
  
  private void computeFPS() {
    long now = System.currentTimeMillis();
    if (now - m_last > 1000L) {
      m_fps = m_npaints;
      m_last = now;
      m_npaints = 0;
    }
    m_game.setFPS(m_fps, null);
    // m_game.setFPS(m_fps, "npaints=" + m_npaints);
    m_npaints++;
  }

  @Override
  protected void _paint(Graphics g) {
	  
	if (m_model.gameMode == 0) {
		
		computeFPS();
		
		//background
		g.drawImage(m_model.m_bg1, 0, 0, getWidth(), getHeight(), null);
		g.drawImage(m_model.m_bg2, 0, 0, getWidth(), getHeight(), null);
		
		Ghost f = new Ghost(m_model, 0, m_model.m_ghostSprite, 4, 4, -100, 100, 10F);
		Ghost f2 = new Ghost(m_model, 0, m_model.m_ghostSprite, 4, 4, getWidth()-220, 100, 10F);
		f.paint(g);
		f2.paint(g);
		
		// TITLE
		Alphabet a;
		for (int i = 0; i < 5; i++) {
			a = new Alphabet(m_model, GHOST[i], m_model.m_alphabet, 3, 9, getWidth()/2 - 265 + 45*i, getHeight()/8, 1F);
			a.paint(g);
			
		}
		
		a = new Alphabet(m_model, 26, m_model.m_alphabet, 3, 9, getWidth()/2 - 30, getHeight()/8, 1F);
		a.paint(g);
		
		for (int j = 0; j < 6; j++) {
			a = new Alphabet(m_model, HUNTER[j], m_model.m_alphabet, 3, 9, getWidth()/2 + 25 + 45*j, getHeight()/8, 1F);
			a.paint(g);
			
		}
		
		TIME++;
		if (TIME < 15) {
			for (int k = 0; k < 5; k++) {
				a = new Alphabet(m_model, CLICK[k], m_model.m_alphabetBLUE, 3, 9, getWidth() / 2 - 140 + 20 * k,
						getHeight() / 8 + 120, 0.4F);
				a.paint(g);
			}

			for (int l = 0; l < 8; l++) {
				a = new Alphabet(m_model, ANYWHERE[l], m_model.m_alphabetBLUE, 3, 9, getWidth() / 2 - 20 + 20 * l,
						getHeight() / 8 + 120, 0.4F);
				a.paint(g);
			}
			
			
		} else {
			if (TIME > 30) {
				TIME = 0;
			}
		}

		
		Platform p = new Platform(getWidth()/2 - 50, getHeight()/2+50, 100, getHeight()/2-50);
		p.img = m_model.m_sol;
		p._paint(g);
		
		Cowboy c = new Cowboy(m_model, 0, m_model.m_cowboySprite, 4, 6, getWidth()/2 - 70, getHeight()/2-70, 3F);
		c.paint(g);
		
	} else if (m_model.gameMode == 1) {
		
	    computeFPS();

	    //background
		g.drawImage(m_model.m_bg1, 0, 0, getWidth(), getHeight(), null);
		g.drawImage(m_model.m_bg2, 0, 0, getWidth(), getHeight(), null);

		//FLOOR
		Platform floor = new Platform(0, 600, getWidth(), getHeight() - 600);
		floor.img = m_model.m_sol;
		floor._paint(g);
		m_model.m_floor = floor.p_y;
				
		// PLATFORM 1
		Platform p1 = new Platform(getWidth()/2 - 300, 4*getHeight()/5 - 70, 200, 30);
		p1.img = m_model.m_sol;
		p1._paint(g);
		
		// PLATFORM 2
		Platform p2 = new Platform(getWidth()/2 + 110, 4*getHeight()/5 - 70, 200, 30);
		p2.img = m_model.m_sol;
		p2._paint(g);

		if (startGame) {
			m_model.m_platforms.add(p1);
			m_model.m_platforms.add(p2);
			startGame = false;
		}
		
		// PAINT THE COWBOY
		Cowboy cowboy = m_model.m_cowboys;
		g.drawRect(cowboy.m_x + 30, cowboy.m_y + 20, cowboy.m_w*(int)cowboy.m_scale - 30, cowboy.m_h*(int)cowboy.m_scale - 15);

		m_model.m_cowboys.heightWindow = getHeight();
		m_model.m_cowboys.widthWindow = getWidth();

		// System.out.println("m_y = " + cowboy.m_y);
		cowboy.paint(g);
		
		// PAINT THE GHOSTS
		Iterator<Ghost> iter2 = m_model.ghosts();
		while (iter2.hasNext()) {
			Ghost f = iter2.next();
			f.heightWindow = getHeight();
			f.widthWindow = getWidth();
			f.paint(g);
		}
		
		// PAINT THE HEARTS
		Heart h;
		for (int i = 0; i < m_model.m_cowboys.NBHEARTS; i++) {
			h = new Heart(10 + 60 * i, 10, 50, 50);
			h.img = m_model.m_heart;
			h._paint(g);
		}
		
		// PAINT THE BULLETS
		Iterator<Bullet> iter3 = m_model.bullets();
		while (iter3.hasNext()) {
			Bullet b = iter3.next();
			b.widthWindow = getWidth();
			b.img = m_model.m_bullet;
			b._paint(g);
		}
		
		
	} else if (m_model.gameMode == 2) {
		
	    computeFPS();

	    //background
		g.drawImage(m_model.m_bg1, 0, 0, getWidth(), getHeight(), null);
		g.drawImage(m_model.m_bg2, 0, 0, getWidth(), getHeight(), null);
		
		Alphabet a;
		for (int k = 0; k < 4; k++) {
			a = new Alphabet(m_model, GAME[k], m_model.m_alphabetBLUE, 3, 9, getWidth() / 2 - 350 + 80 * k,
					getHeight() / 8 + 120, 1.5F);
			a.paint(g);
		}

		for (int l = 0; l < 4; l++) {
			a = new Alphabet(m_model, OVER[l], m_model.m_alphabetBLUE, 3, 9, getWidth() / 2 + 80 * l,
					getHeight() / 8 + 120, 1.5F);
			a.paint(g);
		}		
	}

  }
  
}
