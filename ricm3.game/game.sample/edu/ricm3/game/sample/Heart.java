package edu.ricm3.game.sample;

import java.awt.Graphics;
import java.awt.Image;

import edu.ricm3.game.GameView;

public class Heart extends GameView {
	
	// ATTRIBUTS 
	private static final long serialVersionUID = 1L;
	int p_x, p_y;
	int p_w, p_h;
	Image img;
	
	public Heart(int x, int y, int w, int h) {
		this.p_x = x;
		this.p_y = y;
		this.p_w = w;
		this.p_h = h;
	}
	
	
	@Override
	protected void _paint(Graphics g) {
		g.drawImage(img, p_x, p_y, p_w, p_h, null);
	}
}
