package ricm3.gpi.gui.widgets;

import ricm3.gpi.gui.Font;
import ricm3.gpi.gui.Graphics;
import ricm3.gpi.gui.Image;
import ricm3.gpi.gui.MouseListener;
import ricm3.gpi.gui.Window;
import ricm3.gpi.gui.layout.Component;
import ricm3.gpi.gui.layout.Container;
import ricm3.gpi.gui.layout.Location;

/**
 * A widget that is a simple button than can be clicked.
 * To know about click events on a button, you need to register
 * an ActionListener on that button.
 * 
 * A button can have a label, that is, a name.
 * 
 * A button may also have two images, one for when the button 
 * is pressed down and the other for when the button is released.
 * 
 * 
 * @author Pr. Olivier Gruber  (olivier dot gruber at acm dot org)
 */
public class Button extends Component {

  String m_label;
  Font m_font;
  Image m_pressed;
  Image m_released;
  boolean click;
  ActionListener act;
  
  public Button(Container parent) {
    super(parent);
    setMouseListener(new ClickListener());
    click = false;
  }

  public String getLabel() {
    return m_label;
  }
  
  @Override
  public void paint(Graphics g) {
    super.paint(g);
    if (click) {
      g.drawImage(m_pressed, m_x, m_y, m_width, m_height);
    }
    else {
      g.drawImage(m_released, m_x, m_y, m_width, m_height);
    }
    
    if(m_label != null) {
       g.drawString(m_label, m_x, m_y);
    }
    
  }

  public void setActionListener(ActionListener al) {
    this.act = al;
  }

  public void setFont(Font f) {
    this.m_font = f;
  }

  public void setLabel(String txt) {
    m_label = txt;
    Window win = Window.getWindow();
    m_font = win.font(Window.MONOSPACED, 12F);
  }

  public void setImages(Image released, Image pressed) {
    m_pressed = pressed;
    m_released = released;
  }
  
  class ClickListener implements MouseListener {
	    boolean m_down;
	    String m_msg;

	    ClickListener() {
	      
	    }

	    @Override
	    public void mouseReleased(int x, int y, int buttons) {
	    	if (buttons == 1) {
				Component c = select(x, y);
				if (c instanceof Button) {
					((Button) c).click = false;
					repaint();
				}
	    	}
	    	
	    }

	    @Override
	    public void mousePressed(int x, int y, int buttons) {
	    	if (buttons == 1) {
				Component c = select(x, y);
				if (c instanceof Button) {
					((Button) c).click = true;
					repaint();
				}
	    	}
	    	
	    }

	    @Override
	    public void mouseMoved(int x, int y) {
	   
	    }

	    @Override
	    public void mouseExited() {
	      
	    }

	    @Override
	    public void mouseEntered(int x, int y) {
	     
	    }
	  }

}
