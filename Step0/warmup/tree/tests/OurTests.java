package warmup.tree.tests;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import warmup.tree.Leaf;
import warmup.tree.Node;
import warmup.tree.NotFoundException;
import warmup.tree.Tree;

public class OurTests {
  @Before
  public void before() {
    // Executed before each test
  }

  @After
  public void after() {
    // Executed after each test
  }
  
  @Test
  public void test00() throws NotFoundException {
    Tree tree = new Tree();
    String path;
    Node n1, n2;
    path = "/toto/titi/tutu".replace('/', Tree.pathSeparator);
    n1 = tree.makePath(path, true);
    n2 = tree.makePath(path,true);
    assertTrue(n1 == n2);
  }
  
  @Test
  public void test01() throws NotFoundException {
    Tree tree = new Tree();
    Node n1 = new Node(tree,"a");
    assertTrue(n1.parent()==tree);
    Node n2 = new Node(n1,"b");
    assertTrue(n2.parent()==n1);
    Node n3 = new Node(n2,"c");
    assertTrue(n3.parent()==n2);
    // path exists, but as a node
    // expected to throw IllegalStateException
    String path;
    path = "/a/b/c/b".replace('/', Tree.pathSeparator);
    assertTrue(tree.makePath(path,true) instanceof Leaf);  
  }
  
  @Test
  public void test02() throws NotFoundException {
    Tree tree = new Tree();
    Node n1 = new Node(tree,"toto");
    assertTrue(n1.parent()==tree);
    Node n2 = new Node(n1,"titi");
    assertTrue(n2.parent()==n1);
    Node n3 = new Leaf(n2,"tata"); 
    tree.makePath("/toto/titi/tata", true);
  }
  
  @Test(expected=NotFoundException.class)
  public void test03() throws NotFoundException {
    Tree tree = new Tree();
    String path = "/toto/titi/tata".replace('/', Tree.pathSeparator);
    Node n1 = tree.makePath(path,true);
    n1.remove();
    tree.find("/toto/titi/tata");
  }
  
  public void test04() throws NotFoundException {
    Tree tree = new Tree();
    String path = "/toto/titi/tata".replace('/', Tree.pathSeparator);
    Node n1 = tree.makePath(path, false);
    assertTrue("".equals(tree.name()));
    assertTrue(tree.find(path) == n1);
  }
}
