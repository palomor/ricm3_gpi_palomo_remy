package warmup.tree;

/**
 * This is a tree of named node.
 * 
 * @author Pr. Olivier Gruber  (olivier dot gruber at acm dot org)
 */
public class Tree extends Node {

  public static final char pathSeparator = '/';
  public static final String pathSeparatorString = "/";

  public Tree() {
    super("");
  }

  /**
   * Finds a node corresponding to a path in the tree.
   * If the path does not exists, throws NotFoundException
   *  
   * @param path
   *          of the searched node
   * @return the node named by the given path
   * @throws NotFoundException
   *           if the path does not exist
   */
  public Node find(String path) throws NotFoundException {
	int i = 1;
	if(path.charAt(0) == Tree.pathSeparator) {
		String tab[] = path.split("/");
	    Node racine = this;
	    Node tampon = this;
	    
		while (i < tab.length) {
			tampon = racine.child(tab[i]);
	    	if(tampon != null) {
	    		racine = tampon; 
	    		i++;
	    	}
	    	else throw new NotFoundException(path);
		}
		return tampon;
	}
    
	throw new IllegalArgumentException();
  }

  /**
   * Make a path in the tree, leading either to a leaf or to a node.
   * @throws IllegalStateException
   *         if the path should be to a leaf but it already exists
   *         to a node, or if the path should be to a node but it already
   *         exists to a leaf. 
   */
  public Node makePath(String path, boolean isLeaf) {
	  String[] names = path.split(pathSeparatorString);
	  Node n = this;
	  int i = 1;
	  
	  while(i < names.length && n.child(names[i])!=null) {
		  n=n.child(names[i]);
		  i++;
	  }
  
	  if(i == names.length) {
		  if((n instanceof Leaf) && !isLeaf) {
			  throw new IllegalStateException();
		  } else if (!(n instanceof Leaf) && isLeaf) {
			  throw new IllegalStateException();
		  } else {
			  return n;
		  }
		  
	  } else {
		  while(i < names.length-1) {
			  new Node(n,names[i]);
			  n=n.child(names[i]);
			  i++;
		  }
		  if(isLeaf) {
			  return new Leaf(n,names[i]);
		  } else {
			  return new Node(n,names[i]);
		  }
	  }
    
  }


  public String toString() {
    TreePrinter p = new TreePrinter(this);
    return p.toString();
  }

}
