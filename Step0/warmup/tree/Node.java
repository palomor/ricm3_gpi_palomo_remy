package warmup.tree;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This is a node of a tree.
 * Each node has a name.
 * Therefore, each node is reachable through a path.
 * Each node may be attached an object.
 * 
 * @author Pr. Olivier Gruber  (olivier dot gruber at acm dot org)
 */

public class Node {
  Node m_parent;
  String m_name;
  List<Node> m_children;
  Object m_attachment;

  /**
   * @param name
   * @throws IllegalArgumentException if the name contains 
   *         the character Tree.pathSeparator
   */
  protected Node(String name) {
    
	if (name.contains(Tree.pathSeparatorString)) {
		throw new IllegalArgumentException();
	}
    
    this.m_name = name;
    this.m_children = new LinkedList<Node>();
  }

  /**
   * @param name
   * @throws IllegalArgumentException if the name contains 
   *         the character Tree.pathSeparator
   */
  public Node(Node parent, String name) {
	
	if (name.contains(Tree.pathSeparatorString)) {
		throw new IllegalArgumentException();
	}
	
	if (parent instanceof Leaf) {
		throw new IllegalStateException();
	}
    
    this.m_name = name;
    this.m_parent = parent;
    
    Iterator<Node> iter = parent.children();
    while (iter.hasNext()) {
    	Node courant = iter.next();
    	if (courant.m_name.equals(name)) {
    		throw new IllegalStateException();
    	}
    }
    parent.m_children.add(this);
    this.m_children = new LinkedList<Node>();
  }

  public String toString() {
    if (m_name == null)
      return "";
    return m_name;
  }

  public Node parent() {
    return m_parent;
  }

  public void attach(Object e) {
    m_attachment = e;
  }

  public Object attachment() {
    return m_attachment;
  }

  public void name(String name) {
    m_name = name;
  }

  public String name() {
    return m_name;
  }

  public String path() {
    Node parent = this.m_parent;
    String chemin = Tree.pathSeparatorString + this.m_name;
    
    while (parent != null) {
    	chemin = Tree.pathSeparatorString + parent.m_name + chemin;
    	parent = parent.m_parent;
    }
    
    return chemin;
  }

  public void remove() {
	  Node p = this.m_parent;
	  p.m_children.remove(this);
	  this.m_parent = null;
  }

  public Iterator<Node> children() {
    return m_children.iterator();
  }
  
  public Node child(String name) {
    Iterator<Node> iter = children();
    
    while (iter.hasNext()) {
    	Node courant = iter.next();
    	if (courant.m_name.equals(name)) {
    		return courant;
    	}
    }
    return null;
  }

}
