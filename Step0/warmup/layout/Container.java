package warmup.layout;
import java.util.List;
import java.util.LinkedList;
/**
 * This is a container within a tree of containers and components.
 * A container is a component that has children components.
 * The children are managed as an ordered set.
 * Children components are painted on top of their parent container.
 * 
 * @author Pr. Olivier Gruber (olivier dot gruber at acm dot org)
 */

public class Container extends Component {

  protected List<Component> children;
  
  Container() {
	super();
	this.children = new LinkedList<Component>();
  }

  public Container(Container parent) {
    super(parent);
    this.children = new LinkedList<Component>();
  }

  /**
   * @return the number of components that are 
   *         children to this container
   */
  public int childrenCount() {
	 return this.children.size();
  }

  /**
   * @return the component indexed by the given
   *         index.
   */
  public Component childrenAt(int i) {
    return this.children.get(i);
  }

  /**
   * Select the component, on top, at the given location.
   * The location is given in local coordinates.
   * Reminder: children are above their parent.
   * @param x
   * @param y
   * @return this selected component 
   */
  public Component select(int x, int y) {
	if (!this.inside(x, y)) {
		return null;
	} else {
		Component c = this;
		while (c instanceof Container) {
			int i = 0;

			while (i < ((Container) c).childrenCount() && !((Container) c).childrenAt(i).inside(x, y)) {
				i++;
			}

			if (i == ((Container) c).childrenCount()) {
				return c;
			}

			c = ((Container) c).childrenAt(i);
		}
		return c;
	}

  }

}
