package ricm3.gpi;

import ricm3.gpi.gui.Graphics;
import ricm3.gpi.gui.Color;
import ricm3.gpi.gui.MouseListener;
import ricm3.gpi.gui.KeyListener;
import ricm3.gpi.gui.layout.Component;
import ricm3.gpi.gui.layout.Container;
import ricm3.gpi.gui.layout.Location;
import ricm3.gpi.gui.widgets.Canvas;

import java.util.List;
import java.util.LinkedList;
/**
 * A canvas to draw lines freely.
 * 
 * To draw a line, two possibilities.
 * 
 *  1) Press down the left button and move the mouse
 *     The drawing ends when the left button is released.
 *     
 *  2) Press the space-bar key on the keyboard toggles 
 *     the drawing mode. Moving the mouse draws if 
 *     drawing is on.
 *     
 * To clear the canvas, press the key 'c' on your keyboard.
 * 
 * @author Pr. Olivier Gruber  (olivier dot gruber at acm dot org)
 */
public class DrawCanvas extends Canvas {

  // ATTRIBUTS 
  protected Location start, finish;
  protected List<Location> ligns;
  protected boolean clear = false;
  
  public DrawCanvas(Container parent) {
    super(parent);
    this.setMouseListener(new ClickListener("Canvas"));
    this.setKeyListener(new KeycListener("Canvas"));
    this.ligns = new LinkedList<Location>();
    
  }
  
  // MÉTHODES DE TRAÇAGE ET DE REMISE À ZÉRO
  @Override
  public void paint(Graphics g) {
    g.setColor(m_bgColor);
    Location loc = new Location(0,0);
    g.fillRect(loc.x(), loc.y(), m_width, m_height);
    
    if (this.start != null && this.finish != null) {
    	g.setColor(Color.RED);
    	int i = 0;
    	while (i < this.ligns.size()-1) {
    		g.drawLine(this.ligns.get(i).x(), this.ligns.get(i).y(), this.ligns.get(i+1).x(), this.ligns.get(i+1).y());
    		i += 2;
    	}
    }
    
  }
  
  class ClickListener implements MouseListener {
    boolean m_down;
    String m_msg;

    ClickListener(String msg) {
      m_msg = msg;
    }

    @Override
    public void mouseReleased(int x, int y, int buttons) {
	 if (buttons == 1) {
   	  finish = new Location(x, y);
   	  toLocal(finish);
   	  ligns.add(finish);
   	  repaint();
     }
    }

    @Override
    public void mousePressed(int x, int y, int buttons) {
      m_down = true;
      if (buttons == 1) {
    	  start = new Location(x, y);
    	  toLocal(start);
    	  ligns.add(start);
    	  System.out.println("mousePressed Bro : ("+start.x()+","+finish.y()+")");
      }
    }

    @Override
    public void mouseMoved(int x, int y) {
      System.out.println("mouseMoved Bro : ("+x+","+y+")");
    }

    @Override
    public void mouseExited() {
      System.out.println("mouseExited");
    }

    @Override
    public void mouseEntered(int x, int y) {
      System.out.println("mouseEntered: ("+x+","+y+")");
    }
  }
  
  class KeycListener implements KeyListener {
	  
	  String m_msg;
	  KeycListener(String msg){
		  m_msg = msg;
	  }
	  @Override
	  public void keyPressed(char k, int code) {
	    if (k == 'c') {
	    	ligns.clear();
	    	repaint();
	    }
	  }
	  
	  @Override
	  public void keyReleased(char k, int code) {
	  }
  }
}
